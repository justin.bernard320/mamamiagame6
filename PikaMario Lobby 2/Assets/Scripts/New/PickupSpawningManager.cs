using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PickupSpawningManager : MonoBehaviour
{
    public static PickupSpawningManager instance = null;

    public GameObject starPrefab;
    public bool bStopSpawningOLD = false;
    public float spawnTime = 8.0f;
    public float spawnDelay = 23.0f;

    public bool bStopSpawning = false;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        InvokeRepeating("SpawnStarPickup", spawnTime, spawnDelay);
    }


   // [Server]
    void SpawnStarPickup()
    {
        if (bStopSpawning) { return; }

        Transform startPosition = NetworkManager.singleton.GetStartPosition();

        GameObject newGameObject = Instantiate(starPrefab,
            startPosition.position + new Vector3(0.0f, 1.0f, 0.0f), Quaternion.identity);

        NetworkServer.Spawn(newGameObject);

        //CmdSpawnStars(startPosition);

        if (bStopSpawningOLD)
        {
            CancelInvoke("SpawnStarPickup");
        }
    }

   // [Command]
    void CmdSpawnStars(Transform startPosition)
    {
        //GameObject newGameObject = Instantiate(starPrefab,
            //startPosition.position + new Vector3(0.0f, 1.0f, 0.0f), Quaternion.identity);

        //NetworkServer.Spawn(newGameObject);

        //RpcSpawnStars(startPosition);

    }
  //  [ClientRpc]
    void RpcSpawnStars(Transform startPosition)
    {
        GameObject newGameObject = Instantiate(starPrefab,
            startPosition.position + new Vector3(0.0f, 1.0f, 0.0f), Quaternion.identity);

        NetworkServer.Spawn(newGameObject);
    }

    public void ResetGamePickups()
    {
        GameObject[] stars = GameObject.FindGameObjectsWithTag("Star");
        foreach (GameObject star in stars)
        {
            Destroy(star);
        }

        //InvokeRepeating("SpawnStarPickup", spawnTime, spawnDelay);
    }
}
