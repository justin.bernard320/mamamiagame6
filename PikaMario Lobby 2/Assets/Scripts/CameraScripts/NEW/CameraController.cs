using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : NetworkBehaviour
{
    //[SyncVar]
    GameObject camObject;
    public FreeCameraLook cameraLook;
    public bool bMario;
    //[SyncVar]
    public bool bHideCamera;


    private void Awake()
    {
    }

    public override void OnStartClient()
    {
        bHideCamera = false; //CHANGE FOR CAM

        cameraLook = GameObject.FindGameObjectWithTag("MarioCamera2").GetComponent<FreeCameraLook>();
        StartCoroutine(DelaySpawnEnableCamera());

        //GetComponent<PlayerMovement>().bStopMovement = true;


    }

    IEnumerator DelaySpawnEnableCamera()
    {
        yield return new WaitForSeconds(0.032f);
        bHideCamera = true;
        yield return new WaitForSeconds(0.032f);
        bHideCamera = false;
        yield return new WaitForSeconds(0.032f);
        //bHideCamera = true;
    }

    [Client]
    void Update()
    {
        if (bMario)
        {

            if (bHideCamera)
            {
                //cameraLook.gameObject.SetActive(false);
            }
            else
            {
                cameraLook.gameObject.SetActive(true);
            }
        }
    }
}
