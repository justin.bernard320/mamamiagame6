using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TurnManager : NetworkBehaviour
{
    public static TurnManager instance;

    public float timeStart = 200;

    public int pikachusLeft; //Jared
    public int MariosLeft;
    Text pikachusLeftText;

    public bool bMarioWins = false;
    public bool bPikachuWins = false;

    public GameObject MarioPrefab;
    public GameObject PikachuPrefab;
    public GameObject CursorPrefab;

    public GameObject MarioWinPanel = null;
    public GameObject PikachuWinPanel = null;

    public Player[] PlayersLeft;
    //private GameObject[] allPlayers;

    HealthScriptPikachu[] allPikachuPlayers;
    HealthScript[] allMarioPlayers;

    float EndTimer = 0f;

    public static List<Player> lobbyPlayers = new List<Player>();

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            //Destroy(gameObject);
        }
    }

    public void Start()
    {
        ServerSpawnConnection();

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        MarioWinPanel.SetActive(false);
        PikachuWinPanel.SetActive(false);

        bPikachuWins = false;
    }


    public void AddPlayer(Player _player)
    {
        lobbyPlayers.Add(_player);
        NetworkServer.Spawn(_player.gameObject, _player.connectionToClient);
    }

    public void ServerSpawnConnection()
    {
        StartCoroutine(ServerSpawn());
    }
    [Client]
    IEnumerator ServerSpawn()
    {
        PlayersLeft = FindObjectsOfType<Player>();

        foreach (Player player in PlayersLeft)
        {
            yield return new WaitForSeconds(0.001f);
            player.GetComponent<PlayerSwitch>().SwitchToPikachu();

            yield return new WaitForSeconds(0.010f);
            player.GetComponent<PlayerSwitch>().SwitchToMario();

            yield return new WaitForSeconds(0.001f);
            player.GetComponent<PlayerSwitch>().SwitchToPikachu();

            yield return new WaitForSeconds(0.001f);
            player.GetComponent<PlayerSwitch>().SwitchToMario();

            yield return new WaitForSeconds(0.001f);
            player.GetComponent<PlayerSwitch>().SwitchToPikachu();

            yield return new WaitForSeconds(0.001f);
            player.GetComponent<PlayerSwitch>().SwitchToMario();
        }
    }


    void Update()
    {
        // Debug.Log("MarioPlayerStart: " + MarioPlayerStart.ToString());
        // Debug.Log("NumPlayers: " + numPlayers2.ToString());

        if (pikachusLeftText == null)
        {
            pikachusLeftText = GameObject.Find("PikachusLeftText").GetComponent<Text>();
        }
        if (pikachusLeftText)
        {
            pikachusLeftText.text = "Pikachus Left: " + pikachusLeft.ToString();
        }



        if (pikachusLeft <= 0) //PikachusLeft
        {
            bMarioWins = true;
        }
        else
        {
            bMarioWins = false;
        }


        if (bMarioWins && bPikachuWins == false)
        {
            MarioWinPanel.SetActive(true);
        }
        else
        {
            MarioWinPanel.SetActive(false);
        }

        if (bPikachuWins) //ONLY happens when timer goes to 0!
        {
            PikachuWinPanel.SetActive(true);
        }
        else
        {
            PikachuWinPanel.SetActive(false);
        }

        if (bMarioWins || bPikachuWins)
        {
            EndTimer += Time.deltaTime;
            //Debug.Log(EndTimer.ToString()); //test
        }
        else
        {
            EndTimer = 0;
        }

        if (EndTimer > 6.2f) //change back to 6.2f
        {

            RespawnConnection();

            bPikachuWins = false;
            EndTimer = 0f;

        }
    }



    public void RespawnConnection()
    {
        StartCoroutine(Respawn());
    }

    [Client]
    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(0.32f);

        TimerManager.instance.timeValue = 200;

        bPikachuWins = false;

        PlayersLeft = FindObjectsOfType<Player>();
        // allPlayers = GameObject.FindGameObjectsWithTag("Player");

        List<int> shufflePlayerIndex = new List<int>();


        //if (SceneManager.GetActiveScene().name.StartsWith("CastleScene"))
        {
            //foreach (GameObject player in allPlayers)
            //foreach (Player player in lobbyPlayers) //IDEAL
            //for (var h = 0; h < PlayersLeft.Length; h++)
            for (var h = 0; h < PlayersLeft.Length; h++)
            {
                shufflePlayerIndex.Add(h);
            }

            foreach (Player player in PlayersLeft)
            {
                int randomIndex = UnityEngine.Random.Range(0, shufflePlayerIndex.Count);

                //_player.StartGame(shufflePlayerIndex[randomIndex]);

                if (shufflePlayerIndex[randomIndex] == 0)
                {
                    player.GetComponent<PlayerSwitch>().SwitchToMario();
                }
                else
                {
                    player.GetComponent<PlayerSwitch>().SwitchToPikachu();
                }

                shufflePlayerIndex.RemoveAt(randomIndex);
            }
        }

        RespawnLevelConnector();

    }

    public void RespawnLevelConnector()
    {
        //RespawnLevel();
        StartCoroutine(RespawnLevel());
    }


    //void RespawnLevel()
    [Client]
    IEnumerator RespawnLevel()
    {
        yield return new WaitForSeconds(0.32f);//HAS to be ABOVE 1.32

        pikachusLeft = 0;
        MariosLeft = 0;

        allPikachuPlayers = FindObjectsOfType<HealthScriptPikachu>();
        allMarioPlayers = FindObjectsOfType<HealthScript>();

        foreach (HealthScriptPikachu player in allPikachuPlayers)
        {
            if (player.isActiveAndEnabled)
                pikachusLeft++;
        }
        foreach (HealthScript player in allMarioPlayers)
        {
            if (player.isActiveAndEnabled)
                MariosLeft++;
        }

        if (MariosLeft <= 0)
        {
            TurnManager.instance.RespawnConnection();
        }
    }



    //JARED CODE////////////////////////////
    [Server] //Called from MatchMaker
    public void SetPlayerCount(int playerCount)
    {
        //pikachusLeft = playerCount - 1;
    }

    [Server]
    public void PlayerDied(Player player)
    {
        //Was pikachu, become mario
        pikachusLeft--;
        player.SetPlayerType(PlayerType.Mario);
        if (pikachusLeft <= 0)
        {
            //gameOver
        }
    }
}









//// [Client]
//IEnumerator RespawnNew(Player player) //Mario
//{
//    yield return new WaitForSeconds(1.32f);

//    //Mario Components
//    player.GetComponent<CameraController>().bHideCamera = false;
//    player.GetComponent<PlayerMovement>().bStopMovement = false;

//    numPlayers2++;

//    Debug.Log("NumPlayers: " + numPlayers2.ToString());


//    if (numPlayers2 == 2)
//    {
//        if (player)
//        {
//            //player.GetComponent<PlayerSwitch>().SwitchToMario();
//        }

//    }
//    else
//    {
//        if (player)
//        {
//            //player.GetComponent<PlayerSwitch>().SwitchToPikachu();
//        }
//    }

//    //int MarioChance = Random.Range(1, 2);

//    //if (MarioChance == 1)
//    //{
//    //    player.GetComponent<PlayerSwitch>().SwitchToMario();
//    //}
//    //else
//    //{
//    //    player.GetComponent<PlayerSwitch>().SwitchToPikachu();
//    //}
//}